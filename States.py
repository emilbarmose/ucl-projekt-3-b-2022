import cv2
from statemachine import State
import pygame
import sys
from IMPP import *
import numpy as np
import time
import socket
import os.path

#genreal setup
pygame.init()
clock = pygame.time.Clock()

class Load_Data(State):
    #load data
    def Enter(self):
        #ser om der er en "Data.txt" file, vist der ikke er opretter den en og indskriver grund værdier.
        if os.path.exists('Data.txt') == False: 
            fw = open("Data.txt", "w" )
            fw.write("0" + "\n")
            fw.write("0" + "\n")
            fw.write("0" + "\n")
            fw.write("255" + "\n")
            fw.write("255" + "\n")
            fw.write("255" + "\n")
            fw.write("0" + "\n")
            fw.write("0" + "\n")
            fw.write("0" + "\n")
            fw.close()
        #Læser "Data.txt" filen og indskriver værdierne på globale værdier.
        fr = open("Data.txt", "r")
        frs = fr.read().split('\n')
        self.stateMachine.LB[0] = int(frs[0])
        self.stateMachine.LB[1] = int(frs[1])
        self.stateMachine.LB[2] = int(frs[2])
        self.stateMachine.UB[0] = int(frs[3])
        self.stateMachine.UB[1] = int(frs[4])
        self.stateMachine.UB[2] = int(frs[5])
        self.stateMachine.Antal_Tomater_igår = int(frs[6])
        self.stateMachine.Antal_Tomater_denne_måned = int(frs[7])
        fr.close()
        self.stateMachine.ChangeState(Start_Server())

class Start_Server(State):
    #Initialize server
    def Enter(self):
        self.stateMachine.My_Server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.stateMachine.ChangeState(Listen_Client())

class Listen_Client(State):
    #Wait for client
    def Execute(self):
        
        self.stateMachine.My_Server.bind(('192.168.0.10', 50007))
        self.stateMachine.My_Server.listen(1)

        self.stateMachine.forbindelses_socket, self.stateMachine.addr = self.stateMachine.My_Server.accept()
        #kigger om der er en forbindelse ved at se om addr er ændret fra None
        if self.stateMachine.addr != None:
            self.stateMachine.ChangeState(User_Input())

class User_Input(State):
    def Enter(self):
        #Remove trackbar, and set caption to 'user innput'
        self.stateMachine.thres = HSVThreshold(lowerBound = self.stateMachine.LB, upperBound= self.stateMachine.UB, trackbar=False)
        cv2.destroyAllWindows()
        pygame.display.set_caption('User Input')
    def Execute(self):
        #Gets mouse pos
        mouse = pygame.mouse.get_pos()
        for event in pygame.event.get():
            #Close window if pressed on the "x"
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            #Set a button locatio, looking for a mouse press for "Setup"
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.stateMachine.button_width <= mouse[0] <= self.stateMachine.button_width+140 and self.stateMachine.button_height <= mouse[1] <= self.stateMachine.button_height+40:
                    self.stateMachine.ChangeState(Setup())
            #Set a button locatio, looking for a mouse press for "Quit"
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.stateMachine.qbutton_width <= mouse[0] <= self.stateMachine.qbutton_width+140 and self.stateMachine.qbutton_height <= mouse[1] <= self.stateMachine.qbutton_height+40:
                    self.stateMachine.ChangeState(Save_Data())
            #Set a button locatio, looking for a mouse press for "Move"
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.stateMachine.mbutton_width <= mouse[0] <= self.stateMachine.mbutton_width+140 and self.stateMachine.mbutton_height <= mouse[1] <= self.stateMachine.mbutton_height+40:
                    time.sleep(1)
                    self.stateMachine.ChangeState(Finde_Target())
            #Set a button locatio, looking for a mouse press for "Data"
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.stateMachine.dbutton_width <= mouse[0] <= self.stateMachine.dbutton_width+140 and self.stateMachine.dbutton_height <= mouse[1] <= self.stateMachine.dbutton_height+40:
                    self.stateMachine.ChangeState(Data())
        #Draw and Change coler on button "Data"
        if self.stateMachine.dbutton_width <= mouse[0] <= self.stateMachine.dbutton_width+280 and self.stateMachine.dbutton_height <= mouse[1] <= self.stateMachine.dbutton_height+80:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_light,[self.stateMachine.dbutton_width,self.stateMachine.dbutton_height,280,80])
          
        else:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_dark,[self.stateMachine.dbutton_width,self.stateMachine.dbutton_height,280,80])   
        #Draw and Change coler on button "Quit"
        if self.stateMachine.qbutton_width <= mouse[0] <= self.stateMachine.qbutton_width+280 and self.stateMachine.qbutton_height <= mouse[1] <= self.stateMachine.qbutton_height+80:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_light,[self.stateMachine.qbutton_width,self.stateMachine.qbutton_height,280,80])
          
        else:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_dark,[self.stateMachine.qbutton_width,self.stateMachine.qbutton_height,280,80])      
        #Draw and Change color on button "Move"
        if self.stateMachine.mbutton_width <= mouse[0] <= self.stateMachine.mbutton_width+280 and self.stateMachine.mbutton_height <= mouse[1] <= self.stateMachine.mbutton_height+80:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_light,[self.stateMachine.mbutton_width,self.stateMachine.mbutton_height,280,80])
          
        else:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_dark,[self.stateMachine.mbutton_width,self.stateMachine.mbutton_height,280,80]) 
        #Draw and Change coler on button "Setup"
        if self.stateMachine.button_width <= mouse[0] <= self.stateMachine.button_width+280 and self.stateMachine.button_height <= mouse[1] <= self.stateMachine.button_height+80:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_light,[self.stateMachine.button_width,self.stateMachine.button_height,280,80])
          
        else:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_dark,[self.stateMachine.button_width,self.stateMachine.button_height,280,80])            
        addr = 'conncted to: ' + str(self.stateMachine.addr)
        #Creats a Text
        text = self.stateMachine.mediumfont.render("Setup", True, self.stateMachine.colorLys)
        qtext = self.stateMachine.mediumfont.render("Quit", True, self.stateMachine.colorLys)
        mtext = self.stateMachine.mediumfont.render("Move", True, self.stateMachine.colorLys)
        addtext = self.stateMachine.lowfont.render(addr, True, self.stateMachine.colorLys)
        dtext = self.stateMachine.smallfont.render("Data",True, self.stateMachine.colorLys)

        #Prints the text on the screen
        self.stateMachine.screen.blit(mtext , (self.stateMachine.mbutton_width+65,self.stateMachine.mbutton_height+20)) #Print in loaction(Object,Width,Height)
        self.stateMachine.screen.blit(qtext , (self.stateMachine.qbutton_width+90,self.stateMachine.qbutton_height+20))
        self.stateMachine.screen.blit(text , (self.stateMachine.button_width+70,self.stateMachine.button_height+20))
        self.stateMachine.screen.blit(addtext, (10,10))
        self.stateMachine.screen.blit(dtext, (self.stateMachine.dbutton_width +85, self.stateMachine.dbutton_height+20))
        pygame.display.flip()
        self.stateMachine.screen.blit(self.stateMachine.background,(0,0))
        clock.tick(60)
    def Exit(self):
        #take the vaibel for upper and lower bound and makes them globle as LB and UB
        self.stateMachine.LB = self.stateMachine.thres.lowerBound
        self.stateMachine.UB = self.stateMachine.thres.upperBound

class Setup(State):
    def Enter(self):
        #Set the trackbar active, and the caption to 'setup'
        self.stateMachine.thres = HSVThreshold(lowerBound = self.stateMachine.LB, upperBound= self.stateMachine.UB, trackbar=True)
        pygame.display.set_caption('Setup')
    def Execute(self):
        #getting the mouse pos
        mouse = pygame.mouse.get_pos()
        #checking if someone presses the 'x' on the windowe or if someone is pressing the button on the screen
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.stateMachine.button_width <= mouse[0] <= self.stateMachine.button_width+140 and self.stateMachine.button_height <= mouse[1] <= self.stateMachine.button_height+40:
                    self.stateMachine.ChangeState(User_Input())
        #changes the color of the button if i hover over it
        if self.stateMachine.button_width <= mouse[0] <= self.stateMachine.button_width+140 and self.stateMachine.button_height <= mouse[1] <= self.stateMachine.button_height+40:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_light,[self.stateMachine.button_width,self.stateMachine.button_height,140,40])
          
        else:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_dark,[self.stateMachine.button_width,self.stateMachine.button_height,140,40])            
        #makes the text for the button
        text = self.stateMachine.smallfont.render("save", True, self.stateMachine.colorLys)
        #draw the text on the button
        self.stateMachine.screen.blit(text , (self.stateMachine.button_width+30,self.stateMachine.button_height))
        #update the screen
        pygame.display.update()
        #read the cam
        frame = self.stateMachine.cam.getFrame()
        #Convert the frame to hsv
        hsvframe = self.stateMachine.hsv.run(frame)
        #running the frame throug the thresshold
        Ferdig = self.stateMachine.thres.run(hsvframe)
        #converte frame from a arry to a surface
        background = pygame.pixelcopy.make_surface(Ferdig)
        #flipping the screen
        flip = pygame.transform.rotate(background, 90)
        mirror = pygame.transform.flip(flip, True, False)
        scalef = pygame.transform.scale(mirror,(960,540))
        #print the background on the screen
        self.stateMachine.screen.blit(scalef,(480,0))
        #sets the tickrate
        clock.tick(60)

class Data(State):
    def Execute(self):
        mouse = pygame.mouse.get_pos()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            #Set a button locatio, looking for a mouse press for "Back"
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.stateMachine.dbutton_width <= mouse[0] <= self.stateMachine.dbutton_width+140 and self.stateMachine.dbutton_height <= mouse[1] <= self.stateMachine.dbutton_height+40:
                    self.stateMachine.ChangeState(User_Input())
        #Text
        text = self.stateMachine.smallfont.render("Back",True, self.stateMachine.colorLys)
        idag = self.stateMachine.smallfont.render("Plukket idag: "+str(self.stateMachine.Antal_Tomater_idag),True, self.stateMachine.colorLys)
        igår = self.stateMachine.smallfont.render("Plukket igår: "+str(self.stateMachine.Antal_Tomater_igår),True, self.stateMachine.colorLys)
        Månede = self.stateMachine.smallfont.render("Plukket denne månede: "+str(self.stateMachine.Antal_Tomater_denne_måned),True, self.stateMachine.colorLys)

        #Print on screen background
        self.stateMachine.screen.blit(self.stateMachine.background,(0,0))
        #print on screen Button
        if self.stateMachine.dbutton_width <= mouse[0] <= self.stateMachine.dbutton_width+280 and self.stateMachine.dbutton_height <= mouse[1] <= self.stateMachine.dbutton_height+80:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_light,[self.stateMachine.dbutton_width,self.stateMachine.dbutton_height,280,80])
        else:
            pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_dark,[self.stateMachine.dbutton_width,self.stateMachine.dbutton_height,280,80])
        #print on screen Text
        self.stateMachine.screen.blit(idag, (self.stateMachine.screen_width/2 - 750,self.stateMachine.screen_height/2 - 200))
        self.stateMachine.screen.blit(igår, (self.stateMachine.screen_width/2 - 750,self.stateMachine.screen_height/2 - 250))
        self.stateMachine.screen.blit(Månede, (self.stateMachine.screen_width/2 - 750,self.stateMachine.screen_height/2 - 300))
        self.stateMachine.screen.blit(text, (self.stateMachine.dbutton_width +30 , self.stateMachine.dbutton_height+5))
        pygame.display.flip()
        clock.tick(60)   

class Finde_Target(State):       
    def Execute(self):
        #Sætter den modtaget data fra client ind i variblen
        self.stateMachine.Data = self.stateMachine.forbindelses_socket.recv(4096).decode('ascii')
        #venter på variblen er "be_om_data"
        if self.stateMachine.Data =="be_om_data":
            frame = self.stateMachine.cam.getFrame()
            HSVConverted = self.stateMachine.convertcamera.run(frame)
            self.stateMachine.Center = []
            #finder center for alle de contours og skriver dem i et arry
            for c in HSVConverted:
                M = cv2.moments(c)
                if M["m00"] != 0:
                    cX = int((M["m10"] / M["m00"]))
                    cY = int((M["m01"] / M["m00"]))
                    center = [cX, cY]
                    self.stateMachine.Center.append(center)
                #vist der ikke er nogle center punkter bliver arryet tomt
                else:
                    center = None
            self.stateMachine.billede = cv2.drawContours(frame, HSVConverted, -1, (255, 0, 0), 2)
            #vist arryet for center punkter er tomt, så ændre state til notarget
            if self.stateMachine.Center == []:
                self.stateMachine.ChangeState(No_Target())
            #vist der er et center punkt ændres state til move robot
            if self.stateMachine.Center > []:
                self.stateMachine.ChangeState(Move_Robot())      
    def Exit(self):
        self.stateMachine.Data = None

class No_Target(State):
    def Enter(self):
        self.percount= 0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.stateMachine.ChangeState(User_Input())
        #henter billede ind fra cameraet
        frame = self.stateMachine.cam.getFrame()
        #sætter den globale varibel til billede
        self.stateMachine.billede = frame
        pygame.display.set_caption('No Target')
    def Execute(self):
        #laver en boks til teksten
        pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_dark,[0,self.stateMachine.screen_height/2+50,self.stateMachine.screen_width,100]) 
        #Laver teksten           
        text = self.stateMachine.bigfont.render("No Target", True, self.stateMachine.colorLys)
        #printer teksten på boksen
        self.stateMachine.screen.blit(text , (self.stateMachine.screen_width/2-120,self.stateMachine.screen_height/2+70))
        #updater de nye ting på skærmen    
        pygame.display.update()
        #henter billede ind som baggrund og conventere, flipper, rotere og scalere det så det vender rigtigt
        BGRbackground= cv2.cvtColor(self.stateMachine.billede, cv2.COLOR_BGR2RGB)
        background = pygame.pixelcopy.make_surface(BGRbackground)
        flip = pygame.transform.rotate(background, 90)
        mirror = pygame.transform.flip(flip, True, False)
        scalef = pygame.transform.scale(mirror,(1920,1080))
        #printer baggrunden
        self.stateMachine.screen.blit(scalef,(0,0))
        clock.tick(60)
        #sætter en variabel der køre igennem 10 gange inden den ændre state igen
        self.percount += 1
        if self.percount == 10:
            time.sleep(5)
            self.stateMachine.ChangeState(User_Input())

class Move_Robot(State):
    def Enter(self):
        time.sleep(0.2)
        self.percount=0
        pygame.display.set_caption('Move Robot')
        self.stateMachine.Data = None
        #kigger om der er et center punkt
        if self.stateMachine.Center > []:
            #tager først center punkt i arryet
            self.stateMachine.robkordXZ = self.stateMachine.Center[0]
            #Udregner ud fra x værdien af center punktet, koordinatet x til robotten
            self.stateMachine.robkordX = (((self.stateMachine.robkordXZ[0]*0.38511179)+(self.stateMachine.robkordXZ[1]*0.003352442)-521.7643)/1000)
            #Udregner ud fra y værdien af center punktet, koordinatet y til robotten 
            self.stateMachine.robkordZ = (((self.stateMachine.robkordXZ[0]*-0.003878971)+(self.stateMachine.robkordXZ[1]*0.3773547)+37.49222)/1000)
            #skrive den string robboten skal bruge med koordinater
            self.stateMachine.robkord = ("("+str(self.stateMachine.robkordX)+",-0.865"+','+str(self.stateMachine.robkordZ)+",0.408,2.4,-1.6)")
            #sender stringen til robotten
            self.stateMachine.forbindelses_socket.send(self.stateMachine.robkord.encode('ascii'))
        
    def Execute(self):
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.stateMachine.ChangeState(User_Input())
                
        crosshair_placering = self.stateMachine.Center[0]
        #Tegner en boks til tekst
        pygame.draw.rect(self.stateMachine.screen,self.stateMachine.color_dark,[0,self.stateMachine.screen_height/2-490,self.stateMachine.screen_width,100])
        #laver teksten            
        text = self.stateMachine.bigfont.render("Target Found", True, self.stateMachine.colorLys)
        #printer teksten i boksen
        self.stateMachine.screen.blit(text , (self.stateMachine.screen_width/2-120,self.stateMachine.screen_height/2-470))
        #updater skærmen
        pygame.display.update()
        #henter billede ind som baggrund og conventere, flipper, rotere og scalere det så det vender rigtigt
        BGRbackground= cv2.cvtColor(self.stateMachine.billede, cv2.COLOR_BGR2RGB)
        background = pygame.pixelcopy.make_surface(BGRbackground)
        flip = pygame.transform.rotate(background, 90)
        mirror = pygame.transform.flip(flip, True, False)
        scalef = pygame.transform.scale(mirror,(1920,1080))
        #printer billede
        self.stateMachine.screen.blit(scalef,(0,0))
        #Printer corssair på det center punkt der bliver plukket
        self.stateMachine.screen.blit(self.stateMachine.crosshair,(self.stateMachine.screen_width - crosshair_placering[0]-21, self.stateMachine.screen_height - crosshair_placering[1]-21))
        clock.tick(60)
        #Køre igennem execute 10 gange inden den skifter state og ligger plukket tomater til.
        self.percount += 1
        if self.percount == 10:
            self.stateMachine.ChangeState(Finde_Target())
            self.stateMachine.Antal_Tomater_idag += 1
            self.stateMachine.Antal_Tomater_denne_måned += 1
          
class Stop_Server(State):
    #Shutdown Server
    def Enter(self):
        self.stateMachine.forbindelses_socket.close()
        self.stateMachine.My_Server.close
        self.stateMachine.changeState(Save_Data)

class Save_Data(State):
    def Enter(self):
        #skriver værdierne ned i "Data.txt" inden programmet bliver lukket ned
        fw = open("Data.txt", "w") 
        fw.write(np.array2string(self.stateMachine.LB[0], separator = ' ', suppress_small = True ) + "\n")
        fw.write(np.array2string(self.stateMachine.LB[1], separator = ' ', suppress_small = True ) + "\n")
        fw.write(np.array2string(self.stateMachine.LB[2], separator = ' ', suppress_small = True ) + "\n")
        fw.write(np.array2string(self.stateMachine.UB[0], separator = ',', suppress_small = True ) + "\n")
        fw.write(np.array2string(self.stateMachine.UB[1], separator = ',', suppress_small = True ) + "\n")
        fw.write(np.array2string(self.stateMachine.UB[2], separator = ',', suppress_small = True ) + "\n")
        fw.write(str(self.stateMachine.Antal_Tomater_idag) + "\n")
        fw.write(str(self.stateMachine.Antal_Tomater_denne_måned) + "\n")
        fw.close()
        pygame.quit()
        sys.exit()
