# UCL Projekt 3 B 2022

# Introduktion (DK)
Denne journal har til formål at afdække muligheden for at plukke tomater med en kombineret robot og vision løsning. I dette forsøg vil der blive brugt et OAK – D AI kamera og en UR robot. Løsningen programmeres i Python og gør brug af følgende Libraries; Open-CV, sys, IMPP, pygame, numpy, time og socket. Derudover er der oprettet statemachine.py, OAKWrapper.py og Customstatemachine.py som lokale filer der refereres til. Programmet vil være tilgængeligt på Gitlab under navnet ”UCL projekt 3B 2022”. Projektet er Open-Source og er derved tilgængeligt for alle at hente. 
Tomatplanterne vil blive simuleret med en grøn velcrostrimmel på et hvidt lærred, og tomaterne bliver røde og grønne bolde med velcrostrimler, som kan sættes på planten. 


# Introduction (EN)
This papers serves to uncover the posibillity of picking tomatoes with a combined robot and vision solution. The experiment uses a OAK - D AI camera and a UR Robot, and coded in Python. The following Libraries is used; Open-CV, sys, IMPP, pygame, numpy, time and socket. statemachines.py, OAKWrapper.py and Customstatemachine.py is required as additional files. The project is available at Gitlab under the name ”UCL projekt 3B 2022”. The project is open spurce and therefore available for everyone to download and experiment with. The tomato plants will be simulated with green Velcro Tape, and the tomatoes with red plastic balls with a patch of Velcro tape to stick to the plants.  

# Requirements

This solution requires a OAK - D AI camera and the OAKWrapper.py file in order to work. If you do not have a OAK camera, you'll have to do some testing to make your own camera work. 