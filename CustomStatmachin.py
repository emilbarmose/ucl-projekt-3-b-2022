from IMPP import *
from statemachine import *
import pygame
from OAKWrapper import *
import numpy as np

class Newsm(StateMachine):
    def __init__(self, state):
        super().__init__(state)
        #billede med contur
        self.billede = None
        #Target
        self.crosshair = pygame.image.load("crosshair.png")
        #Center for objekter
        self.Center = [[1,1]]
        #Threshold
        self.LB = np.array([0,0,0])
        self.UB = np.array([255,255,255])
        #Plukkede tomater
        self.Antal_Tomater_idag = 0
        self.Antal_Tomater_igår = 0
        self.Antal_Tomater_denne_måned = 0
        #Camara
        self.cam = OAKCamColor()
        #screen settings
        self.screen_width = 1920
        self.screen_height = 1080
        self.screen = pygame.display.set_mode((self.screen_width,self.screen_height))
        #farve
        self.colorLys = (255,255,255)
        self.color_light = (170,170,170)
        self.color_dark = (100,100,100)
        #font
        self.lowfont = pygame.font.Font("freesansbold.ttf",12)
        self.smallfont = pygame.font.Font("freesansbold.ttf",32) #(Font,str)
        self.mediumfont = pygame.font.Font("freesansbold.ttf",46) #(Font,str)
        self.bigfont = pygame.font.Font("freesansbold.ttf",52) #(Font,str)
        #knap placering
        self.button_width = (self.screen_width/4)*2 + 52
        self.button_height = self.screen_height/2 + 400
        self.qbutton_width = self.screen_width/3 - 12
        self.qbutton_height = self.screen_height/2 + 400
        self.mbutton_width = self.screen_width/6 - 76 
        self.mbutton_height = self.screen_height/2 + 400
        self.dbutton_width = (self.screen_width/4)*3 -44
        self.dbutton_height = self.screen_height/2 + 400
        #Hsv Convetering
        self.hsv = ConvertRGB2HSV()
        #Creat a Background
        self.background = pygame.image.load("Agrobotics_Logo1.png")
        #Pipeline
        self.convertcamera = PostProcessingPipeline([
        AverageBlur(10),
        ConvertRGB2HSV(),
        HSVThreshold(lowerBound= self.LB, upperBound= self.UB, trackbar= False),
        Dilate(size=7, iterations=3),
        Erode(size=7, iterations=3),
        ConvertToGray(showOutput = False, outputWindowName = 'convert to gray'),
        DetectContours(draw = False, drawInfo = ContourDrawInfo((0, 0, 255), 2)),
        ThresholdContours(maxArea= 40000, minArea= 10000)
        ])
        #Server-Client relations
        self.My_Server = None
        self.forbindelses_socket = None
        self.addr = None
        self.robkordXZ = None
        self.robkordX = None
        self.robkordZ = None
        self.robkord = None
        self.Data = None

